package DataHolders;

public class Position implements IPosition {

	private double x, y, z;
	
	public Position ()
	{
		this.setXValue(0);
		this.setYValue(0);
		this.setZValue(0);
	}
	
	public Position (double x, double y, double z)
	{
		this.setXValue(x);
		this.setYValue(y);
		this.setZValue(z);
	}
	
	@Override
	public void setXValue(double x) {
		this.x = x;
	}

	@Override
	public double getXValue() {
		return this.x;
	}

	@Override
	public void setYValue(double y) {
		this.y = y;
	}

	@Override
	public double getYValue() {
		return this.y;
	}

	@Override
	public void setZValue(double z) {
		this.z = z;
	}

	@Override
	public double getZValue() {
		return this.z;
	}
}