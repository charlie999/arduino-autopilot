/**
 * 
 */
package DataHolders;

/**
 * @author Charlie
 *
 */
public class FlightVector implements IVector{
	
	private double MAX_ROLL = 10;
	private double MIN_ROLL = -10;
	private double MAX_YAW = 10;
	private double MIN_YAW = -10;
	private double MAX_PITCH = 10;
	private double MIN_PITCH = -10;
	
	private double roll;
	private double yaw;
	private double pitch;
	
	public FlightVector (double roll, double yaw, double pitch)
	{
		this.setRoll(roll);
		this.setYaw(yaw);
		this.setPitch(pitch);
	}
	
	public FlightVector ()
	{
		this.setRoll(0);
		this.setYaw(0);
		this.setPitch(0);
	}
	
	public void setRoll (double roll) throws IllegalArgumentException
	{
		if ((roll > MAX_ROLL)||(roll < MIN_ROLL))
			throw new IllegalArgumentException();
		this.roll = roll;
	}
	
	public void setYaw (double yaw) throws IllegalArgumentException
	{
		if ((yaw > MAX_YAW)||(yaw < MIN_YAW))
			throw new IllegalArgumentException();
		this.yaw = yaw;
	}
	
	public void setPitch (double pitch) throws IllegalArgumentException
	{
		if ((pitch > MAX_PITCH)||(pitch < MIN_PITCH))
			throw new IllegalArgumentException();
		this.pitch = pitch;
	}
	
	@Override
	public double getRollValue ()
	{
		return this.roll;
	}
	
	@Override
	public double getYawValue ()
	{
		return this.yaw;
	}
	
	@Override
	public double getPitchValue ()
	{
		return this.pitch;
	}
}