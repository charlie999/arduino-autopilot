package DataHolders;

public interface IVector {
	
	public double getRollValue ();
	
	public double getYawValue ();
	
	public double getPitchValue ();
}
