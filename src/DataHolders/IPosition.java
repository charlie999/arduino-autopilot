package DataHolders;

public interface IPosition {

	public void setXValue (double x);
	
	public double getXValue ();
	
	public void setYValue (double y);
	
	public double getYValue ();
	
	public void setZValue (double z);
	
	public double getZValue ();
}
