package com.example.AutoPilot;

import java.util.HashMap;
import java.util.Iterator;
import Exceptions.UsbException;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;

/**
 * @author Charles Hastie
 * @version 1.5
 */
public class USBCommunications
{
	private UsbManager mUsbManager;
	private UsbDeviceConnection conn;
	private UsbEndpoint EPin;
	private UsbEndpoint EPout;
	
	private boolean active = false;
	
	private final int PID;
	private final int VID;
	
	private PermissionReceiver mPermissionReceiver = new PermissionReceiver ();
	private static final String ACTION_USB_PERMISSION = "com.example.AutoPilot.USB";
	
	public USBCommunications (Context context, int VID, int PID)
	{
		this.PID = PID;
		this.VID = VID;
		this.mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		
		//Find attached USB device that matches the arduino PID and VID
		HashMap<String, UsbDevice> devlist = mUsbManager.getDeviceList();
		Iterator<UsbDevice> deviter = devlist.values().iterator();
		while (deviter.hasNext())
		{
			UsbDevice d = deviter.next();
			if (d.getProductId() == this.PID && d.getVendorId() == this.VID)
			{
				if (this.mUsbManager.hasPermission(d))
				{
					this.init(d);
				}
				else
				{
					PendingIntent pi = PendingIntent.getBroadcast(context.getApplicationContext(), 0, new Intent(ACTION_USB_PERMISSION), 0);
					context.registerReceiver(mPermissionReceiver, new IntentFilter(ACTION_USB_PERMISSION));
					this.mUsbManager.requestPermission(d, pi);
				}
				break;
			}
		}
	}
	
	private void init (UsbDevice device)
	{
		this.conn = this.mUsbManager.openDevice(device);
		
		//INITIALISE ARDUINO USB COMMS - 115200 8N1
		this.setLineParameters(115200);
		
		UsbInterface intf = device.getInterface(1);
		
		for (int i = 0; i < intf.getEndpointCount(); i++)
		{
			if (intf.getEndpoint(i).getType() == UsbConstants.USB_ENDPOINT_XFER_BULK)
			{
				if (intf.getEndpoint(i).getDirection() == UsbConstants.USB_DIR_IN)
					this.EPin = intf.getEndpoint(i);
				else
					this.EPout = intf.getEndpoint(i);
			}
		}
		
		try
		{
			this.active = this.initialise('Z');
		}
		catch (UsbException e) {}
	}
	
	/**
	 * Method for setting up the CDC ACM serial converter on the Arduino UNO R3.
	 * @param baudRate
	 * 
	 * Significant sections of this code is based on the work of:
	 * @author Manuel Di Cerbo
	 * @see http://android.serverbox.ch/?p=549
	 * @author mike wakerly
	 * @see https://github.com/mik3y/usb-serial-for-android
	 */
	private void setLineParameters (int baudRate)
	{
		//
		this.conn.controlTransfer(0x21, 34, 0, 0, null, 0, 0);
		//
		byte[] msg = {
                (byte) ( baudRate & 0xff),
                (byte) ((baudRate >> 8 ) & 0xff),
                (byte) ((baudRate >> 16) & 0xff),
                (byte) ((baudRate >> 24) & 0xff),
                0x00,
                0x00,
                0x08};
		this.conn.controlTransfer(0x21, 32, 0, 0, msg, 7, 0);
//		this.conn.controlTransfer(0x21, 32, 0, 0, new byte[] { (byte) 0x80,
//				0x25, 0x00, 0x00, 0x00, 0x00, 0x08 }, 7, 0);

	}
	
	public void send (byte message) throws UsbException
	{
		if (this.conn == null)
		{
			throw new UsbException ("UsbConnection not initialised");
		}
		this.conn.bulkTransfer(this.EPout, new byte[] { message }, 1, 1000);
	}
	
	public void send_string (String message) throws UsbException
	{
		if (this.conn == null)
		{
			throw new UsbException ("UsbConnection not initialised");
		}
		message += '\n';
		this.conn.bulkTransfer(this.EPout, message.getBytes(), message.length(), 1000);
	}
	
	public byte read () throws UsbException
	{
		if (this.conn == null)
		{
			throw new UsbException ("UsbConnection not initialised");
		}
		byte [] temp = new byte [1];
		int data;
		do
		{
			data = this.conn.bulkTransfer(this.EPin, temp, 1, 1000);
			Thread.yield();
		}
		while (data <= 0);
		return temp[0];
	}
	
	public String read_string () throws UsbException
	{
		if (this.conn == null)
		{
			throw new UsbException ("UsbConnection not initialised");
		}
		String message = new String ();
		byte [] data = new byte[64];
		int val = 0;
		
		while (true)
		{
			do
			{
				val = this.conn.bulkTransfer(this.EPin, data, 64, 1000);
				Thread.yield();
			}
			while (val <= 0);
			
			for (int i = 0; i < val; i++)
			{
				if (data[i] == '\n')
					return message;
				message += (char) data[i];
			}
		}
	}
	
	public boolean initialise (char value) throws UsbException
	{
		send_string("IT WORKED");
		char data = (char) read();
		if (data == 'Z')
		{
			return true;
		}
		return false;
	}
	
	public boolean is_active ()
	{
		return this.active;
	}
	
	private class PermissionReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			context.unregisterReceiver(this);
			if (intent.getAction().equals(ACTION_USB_PERMISSION))
			{
				if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false))
				{
					UsbDevice dev = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
					if (dev != null)
					{
						if (dev.getVendorId() == VID && dev.getProductId() == PID)
						{
							init (dev);
						}
					}
				}
			}
		}
	}
}