package com.example.AutoPilot;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

/**
 * @author Charles Hastie
 * @version 1.5
 */
public class SensorService extends Service{

	private final IBinder myBinder = new SensorServiceBinder();
	
	private SensorManager mSensorManager;
	private LocationManager mLocationManager;
	
	private Sensor direction;
	
	private volatile Bundle rotation;
	private volatile Location location;
	
	private SensorEventListener SensorEventListener = new SensorEventListener() {
	    public void onSensorChanged(SensorEvent event)
	    {
	    	if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
	    		update_rotation (event);
	    }
	    
	    public void onAccuracyChanged(Sensor sensor, int accuracy) {}
	};

	private LocationListener LocationListener = new LocationListener() {
		public void onLocationChanged(Location location)
		{
			update_position (location);
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {}

		public void onProviderEnabled(String provider) {}

		public void onProviderDisabled(String provider) {}
	};

	@Override
	public void onCreate ()
	{
		this.mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		this.mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		this.setup_rotation();
		this.setup_position();
	}
	
	@Override
	public void onDestroy ()
	{
		this.mSensorManager.unregisterListener(this.SensorEventListener);
		this.mLocationManager.removeUpdates(this.LocationListener);
	}
	
	@Override
	public IBinder onBind(Intent arg0)
	{
		return myBinder;
	}
	
	public class SensorServiceBinder extends Binder
	{
		public SensorService getService ()
		{
			return SensorService.this;
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		return Service.START_STICKY;
	}
	
	private void setup_position ()
	{
		this.mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				0, 0, LocationListener);
	}
	
	private void setup_rotation ()
	{
		this.direction = this.mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
		
		this.mSensorManager.registerListener(this.SensorEventListener, this.direction,
				SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	private void update_position (Location location)
	{
		this.location = location;
	}
	
	private void update_rotation (SensorEvent event)
	{
		float orientation[] = new float[3], rotation[] = new float[9];
		SensorManager.getRotationMatrixFromVector(rotation, event.values);
		SensorManager.getOrientation(rotation, orientation);
		this.rotation = new Bundle();
		this.rotation.putFloatArray("DATA", orientation);
		this.rotation.putLong("TIME", event.timestamp);
	}
	
	public Location getLocation ()
	{
		return this.location;
	}
	
	public Bundle getRotation ()
	{
		return this.rotation;
	}
}