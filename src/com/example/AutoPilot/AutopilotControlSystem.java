package com.example.AutoPilot;

import java.util.HashMap;

import Exceptions.AutoPilotException;

/**
 * @author Charles Hastie
 * @version 1.5
 */
public abstract class AutopilotControlSystem
{
	public static final String MOTOR = "MOTOR";
	public static final String LEFTAILERON = "LEFTAILERON";
	public static final String RIGHTAILERON = "RIGHTAILERON";
	public static final String RUDDER = "RUDDER";
	
	public static final String ALTITUDE = "ALTITUDE";
	public static final String LONGITUDE = "LONGITUDE";
	public static final String LATITUDE = "LATITUDE";
	
	public static final String ROLL = "ROLL";
	public static final String YAW = "YAW";
	public static final String PITCH = "PITCH";
	
	private long loopTiming;
	private double headingSetPoint = 0;
	private double altitudeSetPoint = 0;
	private double airSpeedSetPoint = 0;
	
	private double motor = 0;
	private double leftAileron = 0;
	private double rightAileron = 0;
	private double rudder = 0;
	
	private double altitude = 0;
	private double longitude = 0;
	private double latitude = 0;
	
	private double roll = 0;
	private double yaw = 0;
	private double pitch = 0;
	
	/**
	 * @param updateTiming update rate in ms
	 */
	public AutopilotControlSystem (long updateTiming)
	{
		this.loopTiming = updateTiming;
	}
	
	public long getLoopTiming ()
	{
		return this.loopTiming;
	}
	
	public void setHeadingSetPoint (double heading)
	{
		this.headingSetPoint = heading;
	}
	
	public void setAltitudeSetPoint (double altitude)
	{
		this.altitudeSetPoint = altitude;
	}
	
	public void setAirSpeedSetPoint (double airSpeed)
	{
		this.airSpeedSetPoint = airSpeed;
	}
	
	
	public void setMotor (double motor) throws AutoPilotException
	{
		if ((motor < 0)||(motor > 255))
			throw new AutoPilotException ("Motor value out of range (0 to 255): " + motor);
		this.motor = motor;
	}
	
	public void setLeftAileron (double leftAileron) throws AutoPilotException
	{
		if ((leftAileron < 0)||(leftAileron > 180))
			throw new AutoPilotException ("Left Aileron value out of range (0 to 180): " + leftAileron);
		this.leftAileron = leftAileron;
	}
	
	public void setRightAileron (double rightAileron) throws AutoPilotException
	{
		if ((rightAileron < 0)||(rightAileron > 180))
			throw new AutoPilotException ("Right Aileron value out of range (0 to 180): " + rightAileron);
		this.rightAileron = rightAileron;
	}
	
	public void setRudder (double rudder) throws AutoPilotException
	{
		if ((rudder < 0)||(rudder > 180))
			throw new AutoPilotException ("Rudder value out of range (0 to 180): " + rudder);
		this.rudder = rudder;
	}
	
	
	
	public void setAltitude (double altitude)
	{
		this.altitude = altitude;
	}
	
	public void setLongitude (double longitude)
	{
		this.longitude = longitude;
	}
	
	public void setLatitude (double latitude)
	{
		this.latitude = latitude;
	}
	
	public void setRoll (double roll)
	{
		this.roll = roll;
	}
	
	public void setYaw (double yaw)
	{
		this.yaw = yaw;
	}
	
	public void setPitch (double pitch)
	{
		this.pitch = pitch;
	}
	
	
	
	public double getAltitudeError ()
	{
		return this.altitudeSetPoint - this.altitude;
	}
	
	public double getHeadingError ()
	{
		return this.headingSetPoint - this.yaw;
	}
	
	public double getAirSpeedError ()
	{
		//TODO calculate airspeed
		return this.airSpeedSetPoint;
	}
	
	public HashMap <String, Double> getAttributes ()
	{
		HashMap <String, Double> attributes = new HashMap <String, Double> ();
		
		attributes.put(MOTOR, (double) motor);
		attributes.put(LEFTAILERON, (double) leftAileron);
		attributes.put(RIGHTAILERON, (double) rightAileron);
		attributes.put(RUDDER, (double) rudder);
		
		attributes.put(ALTITUDE, altitude);
		attributes.put(LATITUDE, latitude);
		attributes.put(LONGITUDE, longitude);
		
		attributes.put(ROLL, roll);
		attributes.put(YAW, yaw);
		attributes.put(PITCH, pitch);
		
		return attributes;
	}
	
	public double getAttibute (String name)
	{
		return this.getAttributes().get(name);
	}
	
	public abstract void updateState (long time, double altitude, double longitude, double latitude, double roll, double yaw, double pitch);
	
	public abstract void updateControl ();
}
