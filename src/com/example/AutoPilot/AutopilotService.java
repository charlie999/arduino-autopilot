package com.example.AutoPilot;

import com.example.AutoPilot.SensorService.SensorServiceBinder;


import Exceptions.AutoPilotException;
import Exceptions.UsbException;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

/**
 * @author Charles Hastie
 * @version 1.5
 */
public class AutopilotService extends Service
{
	
	private final IBinder myBinder = new AutopilotServiceBinder();
	
	private final Handler mHandler = new Handler();
	private Runnable mTimer1;
	
	private SensorService sensorService;
	private boolean sensorBound = false;
	
	private USBCommunications comm;
	
	private AutopilotControlSystem controlSystem;
	
	//AIRCRAFT SENSOR VALUES
	private volatile double Altitude;
	private volatile double Longitude;
	private volatile double Latitude;
	private volatile double Roll, Yaw, Pitch;
	
	private double Altitude_offset;
	private double Longitude_offset;
	private double Latitude_offset;
	
	private volatile boolean gpsStatus = false;
	private volatile boolean rotStatus = false;
	private volatile boolean altStatus = false;
	
	private boolean enabled = false;
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{ 
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return this.myBinder;
	}
	
	public class AutopilotServiceBinder extends Binder {
		public AutopilotService getService ()
		{
			return AutopilotService.this;
		}
	}
	
	@Override
	public void onCreate ()
	{
		Intent intent = new Intent(this, SensorService.class);
        bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
	}
	
	/**
	 * Method to initialise the usb connection, should only be called once the device is connected.
	 * @param VID: Vendor Identification Number
	 * @param PID: Product Identification Number
	 */
	public void init_usb (int VID, int PID)
	{
		this.comm = new USBCommunications (this.getApplicationContext(), VID, PID);
	}
	
	/**
	 * @param control
	 */
	public void setControlSystem (AutopilotControlSystem control)
	{
		this.controlSystem = control;
		
		mTimer1 = new Runnable() {
			@Override
			public void run()
			{
				if (sensorBound)
					update ();
				mHandler.postDelayed(this, controlSystem.getLoopTiming());
			}
		};
		mHandler.postDelayed(mTimer1, controlSystem.getLoopTiming());
	}
	
	public boolean sensorsAvtive ()
	{
		//TODO add gps status
		return this.altStatus&&this.rotStatus;
	}
	
	/**
	 * Method to enable the autopilot control system.
	 * @throws AutoPilotException: if usb communications are inactive (ie unable to control aircraft)
	 */
	public void setEnabled () throws AutoPilotException
	{
		//throw an exception if the autopilot is started without usb being initialised.
		if (!this.comm.is_active())
		{
			throw new AutoPilotException ("USB connection has not been started.");
		}
		this.enabled = true;
	}
	
	public void clearEnabled ()
	{
		this.enabled = false;
	}
	
	public void zeroPosition ()
	{
		if (this.altStatus)
		{
			this.Altitude_offset = this.Altitude;
		}
		if (this.gpsStatus)
		{
			this.Longitude_offset = this.Longitude;
			this.Latitude_offset = this.Latitude;
		}
	}
	
	private void update ()
	{
		//update sensor readings at t (requires controls at t - 1)
		updateSensors ();
		//only update control system if autopilot is enabled
		if (this.enabled)
		{
			//calculate new control positions for t (requires sensor readings at t)
			updateControlSystem();
			//send control position commands over USB
			updateController();
		}
	}
	
	private void updateSensors ()
	{
		this.updateAltitude();
		this.updateLocation();
		this.updateRotation();
	}
	
	private void updateAltitude ()
	{
		if ((this.comm != null)&&(this.comm.is_active()))
		{
			long pressure = Long.parseLong(this.readExternal("BBBBB")); //Pa
			double altitude = SensorManager.getAltitude(
					SensorManager.PRESSURE_STANDARD_ATMOSPHERE,
					((float)pressure) / 100); //m
			this.Altitude = altitude - this.Altitude_offset;
			
			this.altStatus = true;
		}
	}
	
	private void updateLocation ()
	{
		Location temp = this.sensorService.getLocation();
		if (temp != null)
		{
			this.Latitude = temp.getLatitude() - this.Latitude_offset;
			this.Longitude = temp.getLongitude() - this.Longitude_offset;
			
			this.gpsStatus = true;
		}
	}
	
	private void updateRotation ()
	{
		Bundle temp = this.sensorService.getRotation();
		if (temp != null)
		{
			this.Roll = Math.toDegrees(temp.getFloatArray("DATA")[2]);
			this.Yaw = Math.toDegrees(temp.getFloatArray("DATA")[0]);
			this.Pitch = Math.toDegrees(temp.getFloatArray("DATA")[1]);
			
			this.rotStatus = true;
		}
	}
	
	private void updateControlSystem ()
	{
		this.controlSystem.updateState(SystemClock.elapsedRealtime(),
				this.Altitude,
				this.Longitude,
				this.Latitude,
				this.Roll,
				this.Yaw,
				this.Pitch);
		this.controlSystem.updateControl();
	}
	
	private void updateController ()
	{
		if (this.comm.is_active())
		{
			try
			{
				this.comm.send_string("AAAAA");
				this.comm.send((byte)controlSystem.getAttibute(AutopilotControlSystem.MOTOR));
				this.comm.send((byte)controlSystem.getAttibute(AutopilotControlSystem.LEFTAILERON));
				this.comm.send((byte)controlSystem.getAttibute(AutopilotControlSystem.RIGHTAILERON));
				this.comm.send((byte)controlSystem.getAttibute(AutopilotControlSystem.RUDDER));
			}
			catch (UsbException e) {}
		}
	}
	
	/**
	 * Method to request a data value from the attached usb device.
	 * Returns a String that can then be used with the "parse" methods of the various data types.
	 * @param command: instruction to the usb device that determines data to return.
	 * @return a string representing the data value requested.
	 */
	private String readExternal (String command)
	{
		String data = new String ();
		if (this.comm.is_active())
		{
			try
			{
				this.comm.send_string(command);
				data = this.comm.read_string();
			}
			catch (UsbException e) {}
		}
		return data;
	}
	
	private ServiceConnection myConnection = new ServiceConnection() {
	    public void onServiceConnected(ComponentName className,
	            IBinder service) {
	        SensorServiceBinder binder = (SensorServiceBinder) service;
	        sensorService = binder.getService();
	        sensorBound = true;
	    }
	    
	    public void onServiceDisconnected(ComponentName arg0) {
	    	sensorBound = false;
	    }
	};
}


/*
if ((this.comm != null)&&(this.comm.is_active()))
{
	long pressure = Long.parseLong(this.read_external("BBBBB")); //Pa
	double altitude = SensorManager.getAltitude(
			SensorManager.PRESSURE_STANDARD_ATMOSPHERE,
			((float)pressure) / 100); //m
	altitude -= this.Altitude_offset;
	long time = SystemClock.elapsedRealtime();
	if (time > this.AltTime)
	{
		this.dAltitude = (altitude - this.Altitude)
				/ ((time - this.AltTime)*Math.pow(10, -3));
		this.Altitude = altitude;
	}
	
	this.AltTime = time;
	this.altStatus = true;
}
*/

//OLD METHOD USING ON BOARD BAROMETRIC PRESSURE SENSOR
/*
Bundle temp = this.sensorService.getAltitude();
if (temp != null)
{
	double altitude = temp.getDouble("DATA") - this.Altitude_offset;
	long time = temp.getLong("TIME");
	
	if (time > this.AltTime)
	{
		this.dAltitude = (altitude - this.Altitude)
				/ ((time - this.AltTime)*Math.pow(10, -9));
		this.Altitude = altitude;
	}
	
	this.AltTime = time;
	this.altStatus = true;
}
*/

/*
Location temp = this.sensorService.getLocation();
if (temp != null)
{
	double latitude = temp.getLatitude() - this.Latitude_offset;
	double longitude = temp.getLongitude() - this.Longitude_offset;
	long time = temp.getTime();
	
	if (time > this.GPSTime)
	{
		this.dLatitude = (latitude - this.Latitude)
				/ (time - this.GPSTime);
		this.dLongitude = (longitude - this.Longitude)
				/ (time - this.GPSTime);
		this.Latitude = latitude;
		this.Longitude = longitude;
	}
	
	this.gpsStatus = true;
	this.GPSTime = time;
}
*/

/*
Bundle temp = this.sensorService.getRotation();
if (temp != null)
{
	double roll = temp.getFloatArray("DATA")[2];
	double yaw = temp.getFloatArray("DATA")[0];
	double pitch = temp.getFloatArray("DATA")[1];
	long time = temp.getLong("TIME");
	
	if (time > this.RotTime)
	{
		this.dYaw = (yaw - this.Yaw)
				/ ((time - this.RotTime)*Math.pow(10, -9));
		this.dPitch = (pitch - this.Pitch)
				/ ((time - this.RotTime)*Math.pow(10, -9));
		this.dRoll = (roll - this.Roll)
				/ ((time - this.RotTime)*Math.pow(10, -9));
		this.Yaw = yaw;
		this.Pitch = pitch;
		this.Roll = roll;
	}
	
	this.RotTime = time;
	this.rotStatus = true;
}
*/

/*
private void update_external_data ()
{
	if (this.dataOutput != null)
	{
		this.dataOutput.setAltitudeStatus(this.altStatus);
		this.dataOutput.setGPSStatus(this.gpsStatus);
		this.dataOutput.setRotationStatus(this.rotStatus);
		
		this.dataOutput.setLongitude(this.Longitude);
		this.dataOutput.setDeltaLongitude(this.dLongitude);
		this.dataOutput.setLatitude(this.Latitude);
		this.dataOutput.setLatitude(this.dLatitude);
		
		this.dataOutput.setAltitude(this.Altitude);
		this.dataOutput.setDeltaAltitude(this.dAltitude);
		
		this.dataOutput.setRoll(this.Roll);
		this.dataOutput.setDeltaRoll(this.dRoll);
		this.dataOutput.setYaw(this.Yaw);
		this.dataOutput.setDeltaYaw(this.dYaw);
		this.dataOutput.setPitch(this.Pitch);
		this.dataOutput.setDeltaPitch(this.dPitch);
		
		this.dataOutput.setMotorValue(this.Motor);
		this.dataOutput.setLeftAileronValue(this.LAileron);
		this.dataOutput.setRightAileronValue(this.RAileron);
		this.dataOutput.setRudderValue(this.Rudder);
	}
}
*/