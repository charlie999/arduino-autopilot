package com.example.AutoPilot;

public class AircraftParameters
{
	private volatile char Motor;
	private volatile char LAileron, RAileron;
	private volatile char Rudder;
	
	private volatile double Altitude, dAltitude;
	private volatile double Longitude, dLongitude;
	private volatile double Latitude, dLatitude;
	private volatile double Roll, Yaw, Pitch;
	private volatile double dRoll, dYaw, dPitch;
	
	private volatile boolean gpsStatus;
	private volatile boolean rotStatus;
	private volatile boolean altStatus;
	
	public AircraftParameters ()
	{
		this.Altitude = 0;
		this.dAltitude = 0;
		this.Longitude = 0;
		this.dLongitude = 0;
		this.Latitude = 0;
		this.dLatitude = 0;
		
		this.Roll = 0;
		this.dRoll = 0;
		this.Yaw = 0;
		this.dYaw = 0;
		this.Pitch = 0;
		this.dPitch = 0;
		
		this.Motor = 0;
		this.LAileron = 0;
		this.RAileron = 0;
		this.Rudder = 0;
		
		this.gpsStatus = false;
		this.rotStatus = false;
		this.altStatus = false;
	}
	
	public void setAltitude (double altitude)
	{
		this.Altitude = altitude;
	}
	
	/**
	 * Getter for Altitude of aircraft.
	 * @return double representing Altitude of aircraft expressed in metres
	 */
	public double getAltitude ()
	{
		return this.Altitude;
	}
	
	public void setDeltaAltitude (double deltaAltitude)
	{
		this.dAltitude = deltaAltitude;
	}
	
	/**
	 * Getter for rate of change of Altitude of aircraft with respect to time.
	 * @return double representing rate of change of Altitude of aircraft expressed in m/s
	 */
	public double getDeltaAltitude ()
	{
		return this.dAltitude;
	}
	
	public void setLongitude (double longitude)
	{
		this.Longitude = longitude;
	}
	
	/**
	 * Getter for Longitude of aircraft.
	 * @return double representing Longitude of aircraft in ??
	 */
	public double getLongitude ()
	{
		return this.Longitude;
	}
	
	public void setDeltaLongitude (double deltaLongitude)
	{
		this.dLongitude = deltaLongitude;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getDeltaLongitude ()
	{
		return this.dLongitude;
	}
	
	public void setLatitude (double latitude)
	{
		this.Latitude = latitude;
	}
	
	/**
	 * Getter for Latitude of aircraft.
	 * @return double representing Latitude of aircraft in ??
	 */
	public double getLatitude ()
	{
		return this.Latitude;
	}
	
	public void setDeltaLatitude (double deltaLatitude)
	{
		this.dLatitude = deltaLatitude;
	}
	
	/**
	 * @return
	 */
	public double getDeltaLatitude ()
	{
		return this.dLatitude;
	}
	
	public void setRoll (double roll)
	{
		this.Roll = roll;
	}
	
	/**
	 * Getter for Roll of aircraft.
	 * @return double representing Roll of aircraft in radians
	 */
	public double getRoll ()
	{
		return this.Roll;
	}
	
	public void setDeltaRoll (double deltaRoll)
	{
		this.dRoll = deltaRoll;
	}
	
	/**
	 * Getter for rate of change of Roll of aircraft with respect to time.
	 * @return double representing rate of change of Roll of the aircraft in radians/s
	 */
	public double getDeltaRoll ()
	{
		return this.dRoll;
	}
	
	public void setYaw (double yaw)
	{
		this.Yaw = yaw;
	}
	
	/**
	 * Getter for Yaw of aircraft.
	 * @return double representing Yaw of aircraft in radians
	 */
	public double getYaw ()
	{
		return this.Yaw;
	}
	
	public void setDeltaYaw (double deltaYaw)
	{
		this.dYaw = deltaYaw;
	}
	
	/**
	 * Getter for rate of change of Yaw of aircraft with respect to time.
	 * @return double representing rate of change of Yaw of aircraft in radians/s
	 */
	public double getDeltaYaw ()
	{
		return this.dYaw;
	}
	
	public void setPitch (double pitch)
	{
		this.Pitch = pitch;
	}
	
	/**
	 * Getter for Pitch of aircraft.
	 * @return double representing Yaw of aircraft in radians
	 */
	public double getPitch ()
	{
		return this.Pitch;
	}
	
	public void setDeltaPitch (double deltaPitch)
	{
		this.dPitch = deltaPitch;
	}
	
	/**
	 * Getter for rate of change of Pitch of aircraft with respect to time.
	 * @return double representing rate of change of Pitch in radians/s
	 */
	public double getDeltaPitch ()
	{
		return this.dPitch;
	}
	
	public void setMotorValue (char motor)
	{
		this.Motor = motor;
	}
	
	/**
	 * 
	 * @return current control value of the motor
	 */
	public double getMotorValue ()
	{
		double temp = 100 * (((double) this.Motor) / 0xFF);
		return temp;
	}
	
	public void setLeftAileronValue (char lAileron)
	{
		this.LAileron = lAileron;
	}
	
	/**
	 * 
	 * @return current control value of the left aileron
	 */
	public char getLeftAileronValue ()
	{
		return this.LAileron;
	}
	
	public void setRightAileronValue (char rAileron)
	{
		this.RAileron = rAileron;
	}
	
	/**
	 * 
	 * @return current control value of the right aileron
	 */
	public char getRightAileronValue ()
	{
		return this.RAileron;
	}
	
	public void setRudderValue (char rudder)
	{
		this.Rudder = rudder;
	}
	
	/**
	 * @return current control value of the rudder
	 */
	public char getRudderValue ()
	{
		return this.Rudder;
	}
	
	public void setAltitudeStatus (boolean altitudeStatus)
	{
		this.altStatus = altitudeStatus;
	}

	/**
	 * Boolean indicating if the Altitude sensors are active.
	 * @return true if a sensor reading has been accessed
	 */
	public boolean getAltitudeStatus ()
	{
		return this.altStatus;
	}
	
	public void setGPSStatus (boolean gpsStatus)
	{
		this.gpsStatus = gpsStatus;
	}
	
	/**
	 * Boolean indicating if the GPS sensor is active.
	 * @return true if a location has been accessed
	 */
	public boolean getGPSStatus ()
	{
		return this.gpsStatus;
	}
	
	public void setRotationStatus (boolean rotationStatus)
	{
		this.rotStatus = rotationStatus;
	}
	
	/**
	 * Boolean indicating if the rotation sensors are active.
	 * @return true if a sensor reading has been accessed
	 */
	public boolean getRotationStatus ()
	{
		return this.rotStatus;
	}
}
