package com.example.AutoPilot;

import java.util.HashMap;
import Exceptions.AutoPilotException;

/**
 * Interface for autopilot class
 * @author Charles Hastie
 * @version 1.5
 */
public interface IAutoPilot
{
	/**
	 * Method to begin usb communications with aircraft hardware.
	 * Call once the usb device is connected to the phone.
	 * @param VID usb device Vendor ID
	 * @param PID usb device Product ID
	 */
	public void initialiseUSB (int VID, int PID);
	
	/**
	 * Starts the autopilot control system.
	 * @throws AutoPilotException 
	 */
	public void start () throws AutoPilotException;
	
	/**
	 * Stops the autopilot control system.
	 * Autopilot will stop controlling flight surfaces.
	 */
	public void stop ();
	
	/**
	 * Boolean to indicate if the autopilot is functional, indicates if all sensors have responded.
	 * @return true if autopilot is started, false otherwise.
	 */
	public boolean isReady ();
	
	/**
	 * Method to set current location to be the zero point,
	 * such that longitude, latitude, and altitude are taken with reference to this position.
	 * This method should be used to offset the position parameters before launch to ensure sensible altitude values.
	 */
	public void zero ();
	
	/**
	 * Basic setter for desired aircraft yaw (compass heading).
	 * @param heading
	 */
	public void setFlightHeading (double heading) throws AutoPilotException;
	
	/**
	 * Basic setter for desired flight altitude.
	 * @param altitude
	 */
	public void setFlightAltitude (double altitude) throws AutoPilotException;
	
	/**
	 * Simple setter for desired flight speed.
	 * @param speed
	 */
	public void setFlightSpeed (double speed) throws AutoPilotException;
	
	/**
	 * TODO
	 * @return
	 */
	public HashMap<String, Double> getTelemetryData ();
}
