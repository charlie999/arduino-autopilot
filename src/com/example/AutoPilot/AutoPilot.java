/**
 * 
 */
package com.example.AutoPilot;

import java.util.HashMap;

import com.example.AutoPilot.AutopilotService.AutopilotServiceBinder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import Exceptions.AutoPilotException;

/**
 * Startup procedure: TODO revise statup procedure
 * Instantiate an AutoPilot object
 * Once isActive() is true all required subsystems are available
 * Call Start() to begin control system operation
 * @author Charles Hastie
 * @version 1.5
 * @see IAutoPilot
 */
public class AutoPilot implements IAutoPilot
{	
	private final Context application;
	private volatile AutopilotService myService;
	
	private AutopilotControlSystem controlSystem;
	
//	private boolean started = false;
	
	public AutoPilot (Context app, AutopilotControlSystem controlSystem)
	{
		this.application = app;
		
		this.controlSystem = controlSystem;
		
		Intent intent = new Intent(this.application, AutopilotService.class);
		app.bindService(intent, myConnection, Context.BIND_AUTO_CREATE);
	}
	
	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#initialiseUSB(int, int)
	 */
	@Override
	public void initialiseUSB(int VID, int PID)
	{
		this.myService.init_usb(VID, PID);
	}
	
	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#start()
	 */
	@Override
	public void start() throws AutoPilotException
	{
		if (!this.isReady())
		{
			throw new AutoPilotException ("Sensors are inactive.");
		}
//		this.started = true;
		this.myService.setEnabled();
	}

	/* (non-Javadoc)
	 * @see AutoPilot.IAutoPilot#stop()
	 */
	@Override
	public void stop()
	{
//		this.started = false;
		this.myService.clearEnabled();
	}
	
	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#isReady()
	 */
	@Override
	public boolean isReady()
	{
		return (myService != null)&&(myService.sensorsAvtive());
	}
	
	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#zero()
	 */
	@Override
	public void zero ()
	{
		this.myService.zeroPosition();
	}

	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#setFlightHeading(double)
	 */
	@Override
	public void setFlightHeading(double heading) throws AutoPilotException
	{
		//TODO add exception for heading below 0 or above 360
		this.controlSystem.setHeadingSetPoint(heading);
	}
	
	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#setFlightAltitude(double)
	 */
	@Override
	public void setFlightAltitude(double altitude) throws AutoPilotException
	{
		//TODO add exception for altitude below 0
		this.controlSystem.setAltitudeSetPoint(altitude);
	}
	
	/* (non-Javadoc)
	 * @see com.example.AutoPilot.IAutoPilot#setFlightSpeed(double)
	 */
	@Override
	public void setFlightSpeed(double speed) throws AutoPilotException
	{
		//TODO add exception for speed below 0
		this.controlSystem.setAirSpeedSetPoint(speed);
	}
	
	@Override
	public HashMap<String, Double> getTelemetryData()
	{
		HashMap<String, Double> telemetry = new HashMap<String, Double> ();
		for (String key : this.controlSystem.getAttributes().keySet())
			telemetry.put(key, controlSystem.getAttributes().get(key));
		//TODO add sensor data from AutopilotService
		return telemetry;
	}
	
	private ServiceConnection myConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className,
				IBinder service) {
			AutopilotServiceBinder binder = (AutopilotServiceBinder) service;
			myService = binder.getService();
			myService.setControlSystem(controlSystem);
		}

	    public void onServiceDisconnected(ComponentName arg0) {}
	};
}

/*
@Override
public double getFlightSpeed()
{
	return 0;
}

@Override
public IVector getVector()
{
	FlightVector vector = new FlightVector ();
	if (this.temp.getRotationStatus())
	{
		vector.setRoll(this.temp.getRoll());
		vector.setYaw(this.temp.getYaw());
		vector.setPitch(this.temp.getPitch());
	}
	return vector;
}

@Override
public IVector getDotVector()
{
	FlightVector dotVector = new FlightVector ();
	if (this.temp.getRotationStatus())
	{
		dotVector.setRoll(this.temp.getDeltaRoll());
		dotVector.setYaw(this.temp.getDeltaYaw());
		dotVector.setPitch(this.temp.getDeltaPitch());
	}
	return dotVector;
}

@Override
public IPosition getPosition()
{
	Position position = new Position ();
	if (this.temp.getGPSStatus())
	{
		position.setXValue(this.temp.getLongitude());
		position.setYValue(this.temp.getLatitude());
	}
	if (this.temp.getAltitudeStatus())
	{
		position.setZValue(this.temp.getAltitude());
	}
	return position;
}

@Override
public IPosition getDotPosition()
{
	Position dotPosition = new Position ();
	if (this.temp.getGPSStatus())
	{
		dotPosition.setXValue(this.temp.getDeltaLongitude());
		dotPosition.setYValue(this.temp.getDeltaLatitude());
	}
	if (this.temp.getAltitudeStatus())
	{
		dotPosition.setZValue(this.temp.getDeltaAltitude());
	}
	return dotPosition;
}

@Override
public double get_motor ()
{
	return this.temp.getMotorValue();
}

@Override
public char get_LAileron ()
{
	return this.temp.getLeftAileronValue();
}

@Override
public char get_RAileron ()
{
	return this.temp.getRightAileronValue();
}

@Override
public char get_rudder ()
{
	return this.temp.getRudderValue();
}
*/