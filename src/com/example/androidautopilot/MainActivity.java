package com.example.androidautopilot;

import java.text.DecimalFormat;

import Exceptions.AutoPilotException;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.R;
import com.example.AutoPilot.AutoPilot;
import com.example.AutoPilot.AutopilotControlSystem;
import com.example.AutoPilot.IAutoPilot;

/**
 * Dummy activity to test functionality of systems
 * @author Charles Hastie
 * @version 1.5
 */
public class MainActivity extends Activity
{
	private IAutoPilot autoPilot;
	
	private final int VID = 0x2341;
	private final int PID = 0x0043;
	
	private final Handler mHandler = new Handler();
	private Runnable mTimer1;
	
	private TextView usbStatusField;
	private TextView statusField;
	
	private TextView motorField;
	private TextView lAileronField;
	private TextView rAileronField;
	private TextView rudderField;
	
	private TextView longitudeField;
	private TextView latitudeField;
	private TextView altitudeField;
	
	private TextView rollField;
	private TextView yawField;
	private TextView pitchField;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.autoPilot = new AutoPilot (this,
				new AutopilotControlSystem (100)
				{
					@Override
					public void updateState(long time, double altitude,
							double longitude, double latitude, double roll,
							double yaw, double pitch)
					{
						this.setAltitude(altitude);
						this.setLongitude(longitude);
						this.setLatitude(latitude);
						this.setRoll(roll);
						this.setYaw(yaw);
						this.setPitch(pitch);
					}
					
					@Override
					public void updateControl()
					{
						try
						{
							this.setMotor(0);
							this.setRudder(90);
							double roll = this.getAttibute(AutopilotControlSystem.ROLL);
							double pitch = this.getAttibute(AutopilotControlSystem.PITCH);
							if (roll > 15)
							{
								this.setLeftAileron(120);
								this.setRightAileron(60);
							}
							else if (roll < -15)
							{
								this.setLeftAileron(60);
								this.setRightAileron(120);
							}
							else
							{
								if (pitch > 15)
								{
									//positive indicates aircraft should climb.
									this.setLeftAileron(60);
									this.setRightAileron(60);
								}
								else if (pitch < -15)
								{
									this.setLeftAileron(120);
									this.setRightAileron(120);
								}
								else
								{
									this.setLeftAileron(90);
									this.setRightAileron(90);
								}
							}
						}
						catch (AutoPilotException e)
						{
							//TODO print out to screen
						}
					}
				});
		
		this.usbStatusField = (TextView) findViewById(R.id.USB_Status);
		this.statusField = (TextView) findViewById(R.id.Status);
		this.statusField.setText("Inactive");
		
		this.motorField = (TextView) findViewById(R.id.Motor);
		this.lAileronField = (TextView) findViewById(R.id.LAileron);
		this.rAileronField = (TextView) findViewById(R.id.RAileron);
		this.rudderField = (TextView) findViewById(R.id.Rudder);
		
		this.longitudeField = (TextView) findViewById(R.id.Longitude);
		this.latitudeField = (TextView) findViewById(R.id.Latitude);
		this.altitudeField = (TextView) findViewById(R.id.Altitude);
		
		this.rollField = (TextView) findViewById(R.id.Roll);
		this.yawField = (TextView) findViewById(R.id.Yaw);
		this.pitchField = (TextView) findViewById(R.id.Pitch);
		
		((Button)findViewById(R.id.usb_button)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				autoPilot.initialiseUSB(VID, PID);
				usbStatusField.setText("Enabled");
			}
		});
		
		((Button)findViewById(R.id.start_button)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				try
				{
					autoPilot.start();
				} catch (AutoPilotException e) {}
			}
		});
		((Button)findViewById(R.id.stop_button)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{
				autoPilot.stop();
			}
		});
	}

	@Override
	protected void onPause() {
		mHandler.removeCallbacks(mTimer1);
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mTimer1 = new Runnable() {
			@Override
			public void run() {
				update_UI();
				mHandler.postDelayed(this, 100);
			}
		};
		mHandler.postDelayed(mTimer1, 100);
	}
	
	private void update_UI ()
	{
		try
		{
			if (this.autoPilot.isReady())
				statusField.setText("Active");
			motorField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.MOTOR)));
			lAileronField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.LEFTAILERON)));
			rAileronField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.RIGHTAILERON)));
			rudderField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.RUDDER)));
			
			longitudeField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.LONGITUDE)));
			latitudeField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.LATITUDE)));
			altitudeField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.ALTITUDE)));
			
			rollField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.ROLL)));
			yawField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.YAW)));
			pitchField.setText(formatValue(autoPilot.getTelemetryData().get(AutopilotControlSystem.PITCH)));
		}
		catch (Exception e)
		{
			statusField.setText(e.toString());
		}
	}
	
	private String formatValue (double d)
	{
		String value = new String ();
		if (d > 0)
			value += "+";
		else
			if (d == 0)
				value += " ";
			else
				value += "-";
		value += new DecimalFormat("#.#######").format(Math.abs(d));
		return value;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}