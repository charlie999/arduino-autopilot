package Exceptions;

/**
 * Exception class to indicate an illegal or meaningless operation has been called on the AutoPilot.
 * @author Charles Hastie
 * @version 1.3
 */
public class AutoPilotException extends Exception 
{
	private static final long serialVersionUID = 1L;
	private final String message;

	public AutoPilotException (String e)
	{
		message = e;
	}
	
	public String toString ()
	{
		return message;
	}
}
