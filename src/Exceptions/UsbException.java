package Exceptions;

/**
 * Exception class to indicate an illegal operation has been performed with respect to the USB connection.
 * @author Charles Hastie
 * @version 1.3
 */
public class UsbException extends Exception
{
	private static final long serialVersionUID = 1L;
	private String message;
	
	public UsbException (String message)
	{
		this.message = message;
	}
	
	public String toString ()
	{
		return this.message;
	}
}
